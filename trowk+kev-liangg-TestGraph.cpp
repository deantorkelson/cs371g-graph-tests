// -------------
// TestGraph.c++
// -------------

// https://www.boost.org/doc/libs/1_73_0/libs/graph/doc/index.html

// --------
// includes
// --------

#include <iostream> // cout, endl
#include <iterator> // ostream_iterator
#include <sstream>  // ostringstream
#include <utility>  // pair

#include "boost/graph/adjacency_list.hpp" // adjacency_list

#include "gtest/gtest.h"

#include "Graph.hpp"

// ------
// usings
// ------

using namespace std;
using namespace testing;

// ---------
// TestGraph
// ---------

template <typename G>
struct GraphFixture : Test {
    // ------
    // usings
    // ------

    using graph_type          = G;
    using vertex_descriptor   = typename G::vertex_descriptor;
    using edge_descriptor     = typename G::edge_descriptor;
    using vertex_iterator     = typename G::vertex_iterator;
    using edge_iterator       = typename G::edge_iterator;
    using adjacency_iterator  = typename G::adjacency_iterator;
    using vertices_size_type  = typename G::vertices_size_type;
    using edges_size_type     = typename G::edges_size_type;
};

// directed, sparse, unweighted
// possibly connected
// possibly cyclic
using
graph_types =
    Types<
    boost::adjacency_list<boost::setS, boost::vecS, boost::directedS>,
    Graph
    >;

#ifdef __APPLE__
TYPED_TEST_CASE(GraphFixture, graph_types,);
#else
TYPED_TEST_CASE(GraphFixture, graph_types);
#endif

TYPED_TEST(GraphFixture, test0) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor  = typename TestFixture::vertex_descriptor;
    using vertices_size_type = typename TestFixture::vertices_size_type;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);

    vertex_descriptor vd = vertex(0, g);
    ASSERT_EQ(vd, vdA);

    vertices_size_type vs = num_vertices(g);
    ASSERT_EQ(vs, 1u);
}

TYPED_TEST(GraphFixture, test1) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using edges_size_type   = typename TestFixture::edges_size_type;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);

    edge_descriptor edAB = add_edge(vdA, vdB, g).first;

    pair<edge_descriptor, bool> p1 = add_edge(vdA, vdB, g);
    ASSERT_EQ(p1.first,  edAB);
    ASSERT_EQ(p1.second, false);

    pair<edge_descriptor, bool> p2 = edge(vdA, vdB, g);
    ASSERT_EQ(p2.first,  edAB);
    ASSERT_EQ(p2.second, true);

    edges_size_type es = num_edges(g);
    ASSERT_EQ(es, 1u);

    vertex_descriptor vd1 = source(edAB, g);
    ASSERT_EQ(vd1, vdA);

    vertex_descriptor vd2 = target(edAB, g);
    ASSERT_EQ(vd2, vdB);
}

TYPED_TEST(GraphFixture, test2) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using vertex_iterator   = typename TestFixture::vertex_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);

    pair<vertex_iterator, vertex_iterator> p = vertices(g);
    vertex_iterator                        b = p.first;
    vertex_iterator                        e = p.second;
    ASSERT_FALSE(b == e);

    vertex_descriptor vd1 = *b;
    ASSERT_EQ(vd1, vdA);
    ++b;
    ASSERT_FALSE(b == e);

    vertex_descriptor vd2 = *b;
    ASSERT_EQ(vd2, vdB);
    ++b;
    ASSERT_TRUE(b == e);
}

TYPED_TEST(GraphFixture, test3) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using edge_iterator     = typename TestFixture::edge_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    edge_descriptor edAB = add_edge(vdA, vdB, g).first;
    edge_descriptor edAC = add_edge(vdA, vdC, g).first;

    pair<edge_iterator, edge_iterator> p = edges(g);
    edge_iterator                      b = p.first;
    edge_iterator                      e = p.second;
    ASSERT_FALSE(b == e);

    edge_descriptor ed1 = *b;
    ASSERT_EQ(ed1, edAB);
    ++b;
    ASSERT_FALSE(b == e);

    edge_descriptor ed2 = *b;
    ASSERT_EQ(ed2, edAC);
    ++b;
    ASSERT_TRUE(b == e);
}

TYPED_TEST(GraphFixture, test4) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    add_edge(vdA, vdB, g);
    add_edge(vdA, vdC, g);

    pair<adjacency_iterator, adjacency_iterator> p = adjacent_vertices(vdA, g);
    adjacency_iterator                           b = p.first;
    adjacency_iterator                           e = p.second;
    ASSERT_FALSE(b == e);

    vertex_descriptor vd1 = *b;
    ASSERT_EQ(vd1, vdB);
    ++b;
    ASSERT_FALSE(b == e);

    vertex_descriptor vd2 = *b;
    ASSERT_EQ(vd2, vdC);
    ++b;
    ASSERT_TRUE(b == e);
}

//------------
// add_vertex
//------------

TYPED_TEST(GraphFixture, add_vertex_test0) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);

    ASSERT_FALSE(vdA == vdB);
}

TYPED_TEST(GraphFixture, add_vertex_test1) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;

    graph_type g;

    for(int i = 0; i < 10; ++i) {
        ASSERT_TRUE(add_vertex(g) == (vertex_descriptor) i);
    }
}

//--------------
// num_vertices
//--------------

TYPED_TEST(GraphFixture, num_vertices_test0) {
    using graph_type        = typename TestFixture::graph_type;

    graph_type g;

    ASSERT_TRUE(num_vertices(g) == 0u);
}

TYPED_TEST(GraphFixture, num_vertices_test1) {
    using graph_type        = typename TestFixture::graph_type;

    graph_type g;

    for(int i = 0; i < 10; ++i) {
        add_vertex(g);
    }
    ASSERT_TRUE(num_vertices(g) == 10u);
}

TYPED_TEST(GraphFixture, num_vertices_test2) {
    using graph_type        = typename TestFixture::graph_type;

    graph_type g;

    for(int i = 0; i < 1000; ++i) {
        add_vertex(g);
    }
    ASSERT_TRUE(num_vertices(g) == 1000u);
}

//----------
// add_edge
//----------

TYPED_TEST(GraphFixture, add_edge_test0) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);

    pair<edge_descriptor, bool> p0 = add_edge(vdA, vdB, g);
    ASSERT_EQ(p0.second, true);

    pair<edge_descriptor, bool> p1 = add_edge(vdA, vdB, g);
    ASSERT_EQ(p1.first,  p0.first);
    ASSERT_EQ(p1.second, false);
}


//-----------
// num_edges
//-----------

TYPED_TEST(GraphFixture, num_edges_test0) {
    using graph_type        = typename TestFixture::graph_type;

    graph_type g;

    ASSERT_TRUE(num_edges(g) == 0u);
}

TYPED_TEST(GraphFixture, num_edges_test1) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    edge_descriptor edAB = add_edge(vdA, vdB, g).first;
    edge_descriptor edAC = add_edge(vdA, vdC, g).first;
    edge_descriptor edBC = add_edge(vdB, vdC, g).first;

    ASSERT_TRUE(num_edges(g) == 3u);
    ASSERT_NE(edAB, edAC);
    ASSERT_NE(edAB, edBC);
    ASSERT_NE(edAC, edBC);
}

//--------
// vertex
//--------

TYPED_TEST(GraphFixture, vertex_test0) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor  = typename TestFixture::vertex_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);

    vertex_descriptor vd = vertex(0, g);
    ASSERT_EQ(vd, vdA);
}

TYPED_TEST(GraphFixture, vertex_test1) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor  = typename TestFixture::vertex_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);

    vertex_descriptor vd0 = vertex(0, g);
    vertex_descriptor vd1 = vertex(1, g);
    ASSERT_EQ(vd0, vdA);
    ASSERT_EQ(vd1, vdB);
}

TYPED_TEST(GraphFixture, vertex_test2) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor  = typename TestFixture::vertex_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);

    vertex_descriptor vd00 = vertex(0, g);
    vertex_descriptor vd01 = vertex(0, g);
    ASSERT_EQ(vd00, vdA);
    ASSERT_EQ(vd01, vdA);
    ASSERT_EQ(vd00, vd01);
}

//------
// edge
//------

TYPED_TEST(GraphFixture, edge_test0) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    edge_descriptor edAB = add_edge(vdA, vdB, g).first;
    edge_descriptor edAC = add_edge(vdA, vdC, g).first;

    ASSERT_TRUE(num_edges(g) == 2u);
    edge_descriptor ed1 = edge(vdA, vdB, g).first;
    edge_descriptor ed2 = edge(vdA, vdC, g).first;
    bool is_edge3_present = edge(vdB, vdC, g).second;
    ASSERT_EQ(edAB, ed1);
    ASSERT_EQ(edAC, ed2);
    ASSERT_FALSE(is_edge3_present);
}

TYPED_TEST(GraphFixture, edge_test1) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);

    pair<edge_descriptor, bool> p1 = edge(vdA, vdB, g);

    add_edge(vdA, vdB, g);

    pair<edge_descriptor, bool> p2 = edge(vdA, vdB, g);

    ASSERT_NE(p1.first, p2.first);
}

TYPED_TEST(GraphFixture, edge_test2) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    add_edge(vdA, vdA, g);
    add_edge(vdB, vdB, g);
    add_edge(vdC, vdC, g);

    pair<edge_descriptor, bool> p1 = edge(vdA, vdA, g);
    pair<edge_descriptor, bool> p2 = edge(vdB, vdB, g);
    pair<edge_descriptor, bool> p3 = edge(vdC, vdC, g);

    ASSERT_TRUE(p1.second);
    ASSERT_TRUE(p2.second);
    ASSERT_TRUE(p3.second);

    pair<edge_descriptor, bool> p4 = edge(vdA, vdA, g);
    pair<edge_descriptor, bool> p5 = edge(vdB, vdB, g);
    pair<edge_descriptor, bool> p6 = edge(vdC, vdC, g);

    ASSERT_EQ(p1.first, p4.first);
    ASSERT_EQ(p2.first, p5.first);
    ASSERT_EQ(p3.first, p6.first);
}

//-------
// source
//-------

TYPED_TEST(GraphFixture, source_test0) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using edges_size_type   = typename TestFixture::edges_size_type;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    edge_descriptor edAB = add_edge(vdA, vdB, g).first;

    pair<edge_descriptor, bool> p1 = add_edge(vdA, vdB, g);
    ASSERT_EQ(p1.first,  edAB);
    ASSERT_EQ(p1.second, false);

    pair<edge_descriptor, bool> p2 = edge(vdA, vdB, g);
    ASSERT_EQ(p2.first,  edAB);
    ASSERT_EQ(p2.second, true);

    edges_size_type es = num_edges(g);
    ASSERT_EQ(es, 1u);

    vertex_descriptor vd1 = source(edAB, g);
    ASSERT_EQ(vd1, vdA);

    edge_descriptor ed = edge(vdA, vdC, g).first;
    vertex_descriptor vd3 = source(ed, g);
    ASSERT_EQ(vd3, vdA);
}

//-------
// target
//-------

TYPED_TEST(GraphFixture, target_test0) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using edges_size_type   = typename TestFixture::edges_size_type;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    edge_descriptor edAB = add_edge(vdA, vdB, g).first;

    pair<edge_descriptor, bool> p1 = add_edge(vdA, vdB, g);
    ASSERT_EQ(p1.first,  edAB);
    ASSERT_EQ(p1.second, false);

    pair<edge_descriptor, bool> p2 = edge(vdA, vdB, g);
    ASSERT_EQ(p2.first,  edAB);
    ASSERT_EQ(p2.second, true);

    edges_size_type es = num_edges(g);
    ASSERT_EQ(es, 1u);

    vertex_descriptor vd2 = target(edAB, g);
    ASSERT_EQ(vd2, vdB);

    edge_descriptor ed = edge(vdA, vdC, g).first;

    vertex_descriptor vd4 = target(ed, g);
    ASSERT_EQ(vd4, vdC);
}

// ---------------
// vertex iterator
// ---------------

TYPED_TEST(GraphFixture, vertex_it_test0) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using vertex_iterator   = typename TestFixture::vertex_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);
    vertex_descriptor vdD = add_vertex(g);

    pair<vertex_iterator, vertex_iterator> p = vertices(g);
    vertex_iterator                        b = p.first;
    vertex_iterator                        e = p.second;
    ASSERT_FALSE(b == e);

    ASSERT_EQ(vdA, *b);
    ++b;
    ASSERT_FALSE(b == e);

    ASSERT_EQ(vdB, *b);
    ++b;
    ASSERT_FALSE(b == e);

    ASSERT_EQ(vdC, *b);
    ++b;
    ASSERT_FALSE(b == e);

    ASSERT_EQ(vdD, *b);
    ++b;
    ASSERT_TRUE(b == e);
}

TYPED_TEST(GraphFixture, vertex_it_test1) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using vertex_iterator   = typename TestFixture::vertex_iterator;

    graph_type g;

    vertex_descriptor vdArr[100];
    for (int i = 0; i < 100; ++i) {
        vdArr[i] = add_vertex(g);
    }

    pair<vertex_iterator, vertex_iterator> p = vertices(g);
    vertex_iterator                        b = p.first;
    vertex_iterator                        e = p.second;

    for (int i = 0; i < 100; ++i) {
        ASSERT_FALSE(b == e);
        ASSERT_EQ(vdArr[i], *b);
        b++;
    }

    ASSERT_TRUE(b == e);
}

TYPED_TEST(GraphFixture, vertex_it_test2) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using vertex_iterator   = typename TestFixture::vertex_iterator;

    graph_type g;

    vertex_descriptor vdArr[100];
    for (int i = 0; i < 100; ++i) {
        vdArr[i] = add_vertex(g);
    }

    pair<vertex_iterator, vertex_iterator> p = vertices(g);
    vertex_iterator                        b = p.first;
    vertex_iterator                        e = p.second;

    for (int i = 0; i < 100; ++i) {
        ASSERT_FALSE(b == e);
        ASSERT_EQ(vdArr[i], *b);
        b++;
    }

    ASSERT_TRUE(b == e);

    vertex_descriptor vdArr2[100];
    for (int i = 0; i < 100; ++i) {
        vdArr2[i] = add_vertex(g);
    }

    p = vertices(g);
    b = p.first;
    e = p.second;

    for (int i = 0; i < 100; ++i) {
        ASSERT_FALSE(b == e);
        ASSERT_EQ(vdArr[i], *b);
        b++;
    }

    for (int i = 0; i < 100; ++i) {
        ASSERT_FALSE(b == e);
        ASSERT_EQ(vdArr2[i], *b);
        b++;
    }
    ASSERT_TRUE(b == e);
}

// -------------
// edge iterator
// -------------

TYPED_TEST(GraphFixture, edge_it_test0) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using edge_iterator     = typename TestFixture::edge_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    edge_descriptor edAB = add_edge(vdA, vdB, g).first;
    edge_descriptor edAC = add_edge(vdA, vdC, g).first;
    edge_descriptor edBC = add_edge(vdB, vdC, g).first;

    pair<edge_iterator, edge_iterator> p = edges(g);
    edge_iterator                      b = p.first;
    edge_iterator                      e = p.second;
    ASSERT_FALSE(b == e);

    edge_descriptor ed1 = *b;
    ASSERT_EQ(ed1, edAB);
    ++b;
    ASSERT_FALSE(b == e);

    edge_descriptor ed2 = *b;
    ASSERT_EQ(ed2, edAC);
    ++b;
    ASSERT_FALSE(b == e);

    edge_descriptor ed3 = *b;
    ASSERT_EQ(ed3, edBC);
    ++b;
    ASSERT_TRUE(b == e);
}

TYPED_TEST(GraphFixture, edge_it_test1) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using edge_iterator     = typename TestFixture::edge_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    edge_descriptor edAB = add_edge(vdA, vdB, g).first;
    edge_descriptor edAC = add_edge(vdA, vdC, g).first;
    edge_descriptor edBC = add_edge(vdB, vdC, g).first;
    edge_descriptor edBA = add_edge(vdB, vdA, g).first;
    edge_descriptor edCA = add_edge(vdC, vdA, g).first;
    edge_descriptor edCB = add_edge(vdC, vdB, g).first;

    pair<edge_iterator, edge_iterator> p = edges(g);
    edge_iterator                      b = p.first;
    edge_iterator                      e = p.second;
    ASSERT_FALSE(b == e);

    edge_descriptor ed1 = *b;
    ASSERT_EQ(ed1, edAB);
    ++b;
    ASSERT_FALSE(b == e);

    edge_descriptor ed2 = *b;
    ASSERT_EQ(ed2, edAC);
    ++b;
    ASSERT_FALSE(b == e);

    edge_descriptor ed3 = *b;
    ASSERT_EQ(ed3, edBA);
    ++b;
    ASSERT_FALSE(b == e);

    edge_descriptor ed4 = *b;
    ASSERT_EQ(ed4, edBC);
    ++b;
    ASSERT_FALSE(b == e);

    edge_descriptor ed5 = *b;
    ASSERT_EQ(ed5, edCA);
    ++b;
    ASSERT_FALSE(b == e);

    edge_descriptor ed6 = *b;
    ASSERT_EQ(ed6, edCB);
    ++b;
    ASSERT_TRUE(b == e);
}

TYPED_TEST(GraphFixture, edge_it_test2) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using edge_iterator     = typename TestFixture::edge_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);

    edge_descriptor edAB = add_edge(vdA, vdB, g).first;
    edge_descriptor edBA = add_edge(vdB, vdA, g).first;
    edge_descriptor edAA = add_edge(vdA, vdA, g).first;
    edge_descriptor edBB = add_edge(vdB, vdB, g).first;

    pair<edge_iterator, edge_iterator> p = edges(g);
    edge_iterator                      b = p.first;
    edge_iterator                      e = p.second;
    ASSERT_FALSE(b == e);

    edge_descriptor ed1 = *b;
    ASSERT_EQ(ed1, edAA);
    ++b;
    ASSERT_FALSE(b == e);

    edge_descriptor ed2 = *b;
    ASSERT_EQ(ed2, edAB);
    ++b;
    ASSERT_FALSE(b == e);

    edge_descriptor ed3 = *b;
    ASSERT_EQ(ed3, edBA);
    ++b;
    ASSERT_FALSE(b == e);

    edge_descriptor ed4 = *b;
    ASSERT_EQ(ed4, edBB);
    ++b;
    ASSERT_TRUE(b == e);
}

// ------------------
// adjacency iterator
// ------------------

TYPED_TEST(GraphFixture, adjacency_it_test0) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    add_edge(vdA, vdB, g);
    add_edge(vdA, vdC, g);

    pair<adjacency_iterator, adjacency_iterator> p1 = adjacent_vertices(vdB, g);
    adjacency_iterator                           b1 = p1.first;
    adjacency_iterator                           e1 = p1.second;
    ASSERT_TRUE(b1 == e1);

    add_edge(vdB, vdA, g);

    pair<adjacency_iterator, adjacency_iterator> p2 = adjacent_vertices(vdB, g);
    adjacency_iterator                           b2 = p2.first;
    adjacency_iterator                           e2 = p2.second;
    ASSERT_FALSE(b2 == e2);

    vertex_descriptor vd1 = *b2;
    ASSERT_EQ(vd1, vdA);
    ++b2;
    ASSERT_TRUE(b2 == e2);
}

TYPED_TEST(GraphFixture, adjacency_it_test1) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    add_edge(vdB, vdA, g);
    add_edge(vdB, vdC, g);

    pair<adjacency_iterator, adjacency_iterator> p1 = adjacent_vertices(vdB, g);
    adjacency_iterator                           b1 = p1.first;
    adjacency_iterator                           e1 = p1.second;
    ASSERT_FALSE(b1 == e1);

    vertex_descriptor vd1 = *b1;
    ASSERT_EQ(vd1, vdA);

    add_edge(vdA, vdB, g);
    ASSERT_EQ(vd1, vdA);

    ++b1;
    ASSERT_FALSE(b1 == e1);
    vertex_descriptor vd2 = *b1;
    ASSERT_EQ(vd2, vdC);

    add_edge(vdC, vdB, g);
    ASSERT_EQ(vd2, vdC);

    ++b1;
    ASSERT_TRUE(b1 == e1);
}

TYPED_TEST(GraphFixture, adjacency_it_test2) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);

    add_edge(vdA, vdA, g);
    add_edge(vdA, vdA, g);
    add_edge(vdA, vdA, g);

    pair<adjacency_iterator, adjacency_iterator> p1 = adjacent_vertices(vdA, g);
    adjacency_iterator                           b1 = p1.first;
    adjacency_iterator                           e1 = p1.second;
    ASSERT_FALSE(b1 == e1);

    vertex_descriptor vd1 = *b1;
    ASSERT_EQ(vd1, vdA);

    add_edge(vdA, vdA, g);
    ASSERT_EQ(vd1, vdA);

    ++b1;
    ASSERT_TRUE(b1 == e1);
}
